package com.tmendes.birthdaydroid.contact;

public class ContactFactoryException extends RuntimeException {
    public ContactFactoryException(String message) {
        super(message);
    }
}
